console.log("Hello World!");

//.push : add item to the end of an array
let group = ["Eugene","Dennis"];;

group.push("Vincent");
console.log(group);

//.pop : remove item at the end of an array
console.log(group.pop());
console.log(group);

//.unshift : add item at the start of an array
let fruits = ["Mango","Banana"];

fruits.unshift("Apple");
console.log(fruits);

//.shift : removes an item at the start of an array
let computerBrands = ["Apple","Acer","Asus","Dell"];
computerBrands.shift();
console.log(computerBrands);

//.splice : simultaneously remove elements from a specified index and add elements
    //.splice(startingIndex) : all items will be deleted from the starting index
    computerBrands.splice(1);
    console.log(computerBrands);

    //.splice(startingIndex, deleteCount) : delete a specified number of item from a starting index
    fruits.splice(0,1);
    console.log(fruits);

    //.splice(startingIndex, 0, addItem) : add items from the specified starting index
    group.splice(1,0,"Alfred","Jericho");
    console.log(group);

    fruits.splice(0, 2, "Lime","Cherry");
    console.log(fruits);

    let test = "awesome";
    fruits.splice(1,0,test);
    console.log(fruits);

    let fighters = group.splice(0, 1);
    console.log(fighters);
    console.log(group);

//sort : sort element is alphanumeric order
let members = ["Ben","Alan","Alfred","Jino"];
members.sort();
console.log(members);

//.reverse() : reverse the order of array elements
members.reverse();
console.log(members);

//non-mutators methods
// methods that are not able to modify the original array when they are used
let carBrands = ["Vios","Fortuner","Crosswind","City","Vios","Starex"];

// indexOf() : returns the index of the first matching element in the array. return -1 if no match was found
let firstIndexOfVios = carBrands.indexOf("Vios");
console.log(firstIndexOfVios); 

let indexOfStarex = carBrands.indexOf("Starex");
console.log(indexOfStarex);

let indexOfBeetle = carBrands.indexOf("Beetle");
console.log(indexOfBeetle);

//lastIndexOf() : returns the index of the last matching element in the array

let lastIndexOfVios = carBrands.lastIndexOf("Vios");
console.log(lastIndexOfVios);

//slice() : copy a portion of an array and return a new array from it
let shoeBrand = ["Jordan","Nike","Adidas","Converse","Sketchers"];

    //slice(startingIndex) : copy an array to a new array from the starting index
    let myOwnedShoes = shoeBrand.slice(1);
    console.log(myOwnedShoes);

    //slice(startingIndex, endingIndex) : copy an array to a new array from starting index to just before the ending index
    let herOwnedShoes = shoeBrand.slice(2,4);
    console.log(herOwnedShoes);

//toString() : return our array as a string separated by commas
let heroes = ["Captain America","Wiccan","Spiderman","Wanda","Vision","Thor","Doctor Strange"];

let superHeroes = heroes.toString();
console.log(superHeroes);
console.log("My favorite heroes are " +superHeroes);

// join() : return the array as a string with specified separator
let superHeroes1 = heroes.join(" ")
console.log(superHeroes1);    

let superHeroes2 = heroes.join("||");
console.log(superHeroes2);

// iterator methods : iterates or loops over the items in an array
// forEach() : similar to a for loop wherein it is able to iterate over thhe items in an array. Able to repeat an action FOR EACH item in an array
// takes an argument which is a function
//the function inside forEach is able to receive the current item being looped
let counter = 0;
heroes.forEach(function(hero){
    counter++;
    console.log(counter);
    console.log(hero);
})

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

chessBoard.forEach(function(row){
    row.forEach(function(cells){
        console.log(cells);
    });
}) 

let number = [5,21,4,80,15,2];
number.forEach(function(num){
    if(num % 5 === 0){
        console.log(num + " is divisible by 5");
    } else{
        console.log(num +" is not divisible by 5");
    }
})

// map() : similar to forEach, it will iterate over all items in an array and run a function for each items. However, with map, whatever is returned in the function will be added into a new array
let awesomePeople = members.map(function(member){
    return member + " is an awesome person";
})
console.log(awesomePeople); //returns a new array

let coolPeople = members.forEach(function(member){
    return member + " is a cool person";
})
console.log(coolPeople); //returns undefined

// includes() : returns bool which determines if the item is in the array or not
let isMember = members.includes("Ben");
console.log(isMember);